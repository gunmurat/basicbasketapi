package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/shopping-app/controllers"
	"gitlab.com/gunmurat/shopping-app/repository"
	"gitlab.com/gunmurat/shopping-app/service"
)

var basketController = controllers.NewBasketController(service.NewBasketService(repository.NewBasketRepository("baskets")))

func BasketRoutes(app *fiber.App) {
	app.Post("baskets/:basketID", basketController.AddProductToBasket)
	app.Delete("baskets/:basketID/products/:productId", basketController.RemoveProductFromBasket)
	app.Get("/baskets/:basketID", basketController.GetBasket)
	app.Post("/baskets", basketController.CreateBasicBasket)
	app.Get("/baskets", basketController.GetAllBaskets)
}
