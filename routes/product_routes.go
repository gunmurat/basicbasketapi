package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/shopping-app/controllers"
	"gitlab.com/gunmurat/shopping-app/repository"
	"gitlab.com/gunmurat/shopping-app/service"
)

var productController = controllers.NewProductController(service.NewProductService(repository.NewProductRepository("products")))

func ProductRoutes(app *fiber.App) {
	app.Get("/products", productController.GetAllProducts)
	app.Get("/products/:id", productController.GetProduct)
	app.Post("/products", productController.CreateProduct)
}
