package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Basket struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	TotalAmount float64            `json:"totalAmount" bson:"totalAmount"`
	Items       []BasketItem       `json:"items" bson:"items"`
}

func NewBasicBasket() Basket {
	return Basket{
		TotalAmount: 0,
		Items:       []BasketItem{},
	}
}
