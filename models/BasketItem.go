package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type RequestModel struct {
	ProductID primitive.ObjectID `json:"productID" bson:"productID"`
	Quantity  int                `json:"quantity" bson:"quantity"`
}
type BasketItem struct {
	Product  Product `json:"product" bson:"product"`
	Quantity int     `json:"quantity" bson:"quantity"`
	Price    int     `json:"price" bson:"price"`
}

func SetPrice(item BasketItem) {
	item.Price = int(item.Product.Price * float64(item.Quantity))
}
