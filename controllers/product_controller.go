package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/shopping-app/service"
)

type ProductController interface {
	GetAllProducts(ctx *fiber.Ctx) error
	GetProduct(ctx *fiber.Ctx) error
	CreateProduct(ctx *fiber.Ctx) error
}

type ProductControllerImpl struct {
	service service.ProductService
}

func NewProductController(service service.ProductService) ProductController {
	return &ProductControllerImpl{
		service: service,
	}
}

func (controller *ProductControllerImpl) GetAllProducts(ctx *fiber.Ctx) error {
	products, err := controller.service.GetAllProducts()
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"message": err.Error(),
			"success": false,
		})
	}
	return ctx.Status(200).JSON(fiber.Map{
		"success": true,
		"data":    products,
	})
}

func (controller *ProductControllerImpl) GetProduct(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	product, err := controller.service.GetProduct(id)
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"message": err.Error(),
			"success": false,
		})
	}
	return ctx.Status(200).JSON(fiber.Map{
		"success": true,
		"data":    product,
	})
}

func (controller *ProductControllerImpl) CreateProduct(ctx *fiber.Ctx) error {
	body := ctx.Body()
	product, err := controller.service.CreateProduct(body)
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"message": err.Error(),
			"success": false,
		})
	}
	return ctx.Status(200).JSON(fiber.Map{
		"success": true,
		"data":    product,
	})
}
