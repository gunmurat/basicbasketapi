package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/shopping-app/service"
)

type BasketController interface {
	AddProductToBasket(ctx *fiber.Ctx) error
	GetBasket(ctx *fiber.Ctx) error
	CreateBasicBasket(ctx *fiber.Ctx) error
	RemoveProductFromBasket(ctx *fiber.Ctx) error
	GetAllBaskets(ctx *fiber.Ctx) error
}

type BasketControllerImpl struct {
	service service.BasketService
}

func NewBasketController(service service.BasketService) BasketController {
	return &BasketControllerImpl{
		service: service,
	}
}

func (controller *BasketControllerImpl) AddProductToBasket(ctx *fiber.Ctx) error {
	basketID := ctx.Params("basketID")

	success, err := controller.service.AddProductToBasket(basketID, ctx.Body())
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"message": err.Error(),
			"success": false,
		})
	}
	return ctx.Status(200).JSON(fiber.Map{
		"success": success,
	})
}

func (controller *BasketControllerImpl) GetBasket(ctx *fiber.Ctx) error {
	basketID := ctx.Params("basketID")
	basket, err := controller.service.GetBasket(basketID)
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"message": err.Error(),
			"success": false,
		})
	}
	return ctx.Status(200).JSON(fiber.Map{
		"success": true,
		"data":    basket,
	})
}

func (controller *BasketControllerImpl) CreateBasicBasket(ctx *fiber.Ctx) error {
	basket, err := controller.service.CreateBasicBasket()
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"message": err.Error(),
			"success": false,
		})
	}
	return ctx.Status(200).JSON(fiber.Map{
		"success": true,
		"data":    basket,
	})
}

func (controller *BasketControllerImpl) RemoveProductFromBasket(ctx *fiber.Ctx) error {
	basketID := ctx.Params("basketID")
	productId := ctx.Params("productId")
	success, err := controller.service.RemoveProductFromBasket(basketID, productId)
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"message": err.Error(),
			"success": false,
		})
	}
	return ctx.Status(200).JSON(fiber.Map{
		"success": success,
	})
}

func (controller *BasketControllerImpl) GetAllBaskets(ctx *fiber.Ctx) error {
	baskets, err := controller.service.GetAllBaskets()
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"message": err.Error(),
			"success": false,
		})
	}
	return ctx.Status(200).JSON(fiber.Map{
		"success": true,
		"data":    baskets,
	})
}
