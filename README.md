# BasicBasketAPI

Basic Basket API that is developed with Golang.

# Description

This repo includes a Basic Basket API that is developed with Go Fiber v2 and MongoDB.

# RUN
```
go run main.go
```

# TEST
````
go test ./test
````
