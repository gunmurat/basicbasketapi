package repository

import (
	"context"
	"encoding/json"
	"errors"

	"gitlab.com/gunmurat/shopping-app/config"
	"gitlab.com/gunmurat/shopping-app/helpers"
	"gitlab.com/gunmurat/shopping-app/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type BasketRepository interface {
	AddProductToBasket(basketID string, body []byte) (bool, error)
	RemoveProductFromBasket(basketID string, productId string) (bool, error)
	GetBasket(basketID string) (*models.Basket, error)
	CreateBasicBasket() (*models.Basket, error)
	GetAllBaskets() ([]models.Basket, error)
}

type BasketRepositoryImpl struct {
	collection string
	products   string
}

func NewBasketRepository(collection string) BasketRepository {
	return &BasketRepositoryImpl{
		collection: collection,
		products:   "products",
	}
}

func (repository *BasketRepositoryImpl) AddProductToBasket(basketID string, body []byte) (bool, error) {
	var basket models.Basket
	var product models.Product
	var basketItem models.BasketItem
	var requestModel models.RequestModel

	err := json.Unmarshal(body, &requestModel)
	if err != nil {
		return false, err
	}

	hexBasket, errN := primitive.ObjectIDFromHex(basketID)
	if errN != nil {
		return false, errN
	}
	data := config.GetCollection(repository.collection).FindOne(context.TODO(), bson.M{"_id": hexBasket})
	if data.Err() == mongo.ErrNoDocuments {
		return false, errors.New("Basket not found with id: " + basketID)
	}

	if data.Err() != nil {
		return false, data.Err()
	}

	err = data.Decode(&basket)

	if err != nil {
		return false, errors.New("basket model can not decode")
	}

	data = config.GetCollection(repository.products).FindOne(context.TODO(), bson.M{"_id": requestModel.ProductID})
	if data.Err() == mongo.ErrNoDocuments {
		return false, errors.New("Product not found with id: " + requestModel.ProductID.Hex())
	}

	if data.Err() != nil {
		return false, data.Err()
	}

	err = data.Decode(&product)

	if err != nil {
		return false, errors.New("product model can not decode")
	}

	basketItem.Product = product
	basketItem.Quantity = requestModel.Quantity
	basketItem.Price = int(product.Price * float64(requestModel.Quantity))

	if len(basket.Items) > 0 {
		for index, item := range basket.Items {
			if item.Product.ID == basketItem.Product.ID {
				basket.Items[index].Quantity += basketItem.Quantity
				basket.Items[index].Price = int(basket.Items[index].Product.Price) * basket.Items[index].Quantity
				basket.TotalAmount += float64(basket.Items[index].Price)
			} else {
				if index == len(basket.Items)-1 {
					basket.Items = append(basket.Items, basketItem)
					basket.Items[index].Price = int(basket.Items[index].Product.Price) * basket.Items[index].Quantity
					basket.TotalAmount += float64(basket.Items[index].Price)
				}
			}
		}
	} else {
		basket.Items = append(basket.Items, basketItem)
		basket.Items[0].Price = int(basket.Items[0].Product.Price) * basket.Items[0].Quantity
		basket.TotalAmount += float64(basket.Items[0].Price)
	}

	_, err = config.GetCollection(repository.collection).UpdateOne(context.TODO(), bson.M{"_id": hexBasket}, bson.M{"$set": basket})
	if err != nil {
		return false, err
	}

	return true, nil
}

func (repository *BasketRepositoryImpl) GetBasket(basketID string) (*models.Basket, error) {
	var basket models.Basket

	hex, err := primitive.ObjectIDFromHex(basketID)
	if err != nil {
		return nil, err
	}
	data := config.GetCollection(repository.collection).FindOne(context.TODO(), bson.M{"_id": hex})
	if data.Err() == mongo.ErrNoDocuments {
		return nil, errors.New("Basket not found with id: " + basketID)
	}

	if data.Err() != nil {
		return nil, data.Err()
	}

	err = data.Decode(&basket)

	if err != nil {
		return nil, errors.New("basket model can not decode")
	}

	return &basket, nil

}

func (repository *BasketRepositoryImpl) CreateBasicBasket() (*models.Basket, error) {
	var basket = models.Basket{
		Items:       []models.BasketItem{},
		TotalAmount: 0,
	}

	res, err := config.GetCollection(repository.collection).InsertOne(context.TODO(), basket)
	if err != nil {
		return nil, errors.New("error when creating shopping basket")
	}

	basket.ID = res.InsertedID.(primitive.ObjectID)

	return &basket, nil
}

func (repository *BasketRepositoryImpl) RemoveProductFromBasket(basketID string, productId string) (bool, error) {
	basket, err := repository.GetBasket(basketID)
	if err != nil {
		return false, err
	}

	for index, item := range basket.Items {
		if item.Product.ID.Hex() == productId {
			basket.Items = append(basket.Items[:index], basket.Items[index+1:]...)
			basket.TotalAmount -= float64(item.Price)
		}
	}

	_, err = config.GetCollection(repository.collection).UpdateOne(context.TODO(), bson.M{"_id": basket.ID}, bson.M{"$set": basket})
	if err != nil {
		return false, err
	}

	return true, nil
}

func (repository *BasketRepositoryImpl) GetAllBaskets() ([]models.Basket, error) {
	baskets, err := config.GetCollection(repository.collection).Find(context.TODO(), helpers.SFilter, helpers.Opt)

	if err != nil {
		return nil, err
	}

	var basketList []models.Basket

	for baskets.Next(context.TODO()) {
		var basket models.Basket
		err = baskets.Decode(&basket)
		if err != nil {
			return nil, err
		}
		basketList = append(basketList, basket)
	}

	return basketList, nil
}
