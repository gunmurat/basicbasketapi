package repository

import (
	"context"
	"encoding/json"
	"errors"

	"gitlab.com/gunmurat/shopping-app/config"
	"gitlab.com/gunmurat/shopping-app/helpers"
	"gitlab.com/gunmurat/shopping-app/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type ProductRepository interface {
	GetAllProducts() ([]models.Product, error)
	GetProduct(id string) (*models.Product, error)
	CreateProduct(body []byte) (*models.Product, error)
}

type ProductRepositoryImpl struct {
	collection string
}

func NewProductRepository(collection string) ProductRepository {
	return &ProductRepositoryImpl{
		collection: collection,
	}
}

func (repository *ProductRepositoryImpl) GetAllProducts() ([]models.Product, error) {
	var products []models.Product

	cursor, err := config.GetCollection(repository.collection).Find(context.TODO(), helpers.SFilter, helpers.Opt)
	if err != nil {
		return nil, err
	}

	for cursor.Next(context.TODO()) {
		var product models.Product
		err := cursor.Decode(&product)
		if err != nil {
			return nil, err
		}
		products = append(products, product)
	}

	return products, nil
}

func (repository *ProductRepositoryImpl) GetProduct(id string) (*models.Product, error) {
	var product models.Product

	hex, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}
	data := config.GetCollection(repository.collection).FindOne(context.TODO(), bson.M{"_id": hex})
	if data.Err() == mongo.ErrNoDocuments {
		return nil, errors.New("Product not found with id: " + id)
	}

	if data.Err() != nil {
		return nil, data.Err()
	}

	err = data.Decode(&product)

	if err != nil {
		return nil, errors.New("product can not decode")
	}

	return &product, nil

}

func (repository *ProductRepositoryImpl) CreateProduct(body []byte) (*models.Product, error) {
	var product models.Product

	err := json.Unmarshal(body, &product)
	if err != nil {
		return nil, err
	}

	res, err := config.GetCollection(repository.collection).InsertOne(context.TODO(), product)
	if err != nil {
		return nil, err
	}

	product.ID = res.InsertedID.(primitive.ObjectID)

	return &product, nil
}
