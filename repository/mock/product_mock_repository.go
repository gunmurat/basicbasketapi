package mock

import (
	"encoding/json"

	"gitlab.com/gunmurat/shopping-app/models"
	"gitlab.com/gunmurat/shopping-app/repository"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type MockedProductRepositoryImpl struct {
	collection string
}

func NewMockedProductRepository(collection string) repository.ProductRepository {
	return &MockedProductRepositoryImpl{
		collection: collection,
	}
}

func (repository *MockedProductRepositoryImpl) GetAllProducts() ([]models.Product, error) {
	return []models.Product{
		{
			ID:          primitive.NewObjectID(),
			Name:        "Product 1",
			Description: "Product 1 description",
			Price:       10,
			Stock:       10,
		},
		{
			ID:          primitive.NewObjectID(),
			Name:        "Product 2",
			Description: "Product 2 description",
			Price:       20,
			Stock:       20,
		},
	}, nil
}

func (repository *MockedProductRepositoryImpl) GetProduct(id string) (*models.Product, error) {

	hex, _ := primitive.ObjectIDFromHex(id)

	return &models.Product{
		ID:          hex,
		Name:        "Product 1",
		Description: "Product 1 description",
		Price:       10,
		Stock:       10,
	}, nil
}

func (repository *MockedProductRepositoryImpl) CreateProduct(body []byte) (*models.Product, error) {

	var product models.Product

	err := json.Unmarshal(body, &product)

	if err != nil {
		return nil, err
	}

	return &product, nil

}
