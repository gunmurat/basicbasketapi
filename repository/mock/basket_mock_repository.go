package mock

import (
	"encoding/json"

	"gitlab.com/gunmurat/shopping-app/models"
	"gitlab.com/gunmurat/shopping-app/repository"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type MockedBasketRepositoryImpl struct {
	collection string
	products   string
}

func NewMockedBasketRepository(collection string) repository.BasketRepository {
	return &MockedBasketRepositoryImpl{
		collection: collection,
		products:   "products",
	}
}

func (repository *MockedBasketRepositoryImpl) AddProductToBasket(basketID string, body []byte) (bool, error) {

	err := json.Unmarshal(body, &models.BasketItem{})

	if err != nil {
		return false, err
	}

	return true, nil
}

func (repository *MockedBasketRepositoryImpl) RemoveProductFromBasket(basketID string, productId string) (bool, error) {

	return true, nil
}

func (repository *MockedBasketRepositoryImpl) GetBasket(basketID string) (*models.Basket, error) {

	hex, _ := primitive.ObjectIDFromHex(basketID)

	return &models.Basket{
		ID: hex,
		Items: []models.BasketItem{
			{
				Product: models.Product{
					ID:          primitive.NewObjectID(),
					Name:        "Product 1",
					Description: "Product 1 description",
					Price:       10,
					Stock:       10,
				},
				Quantity: 1,
				Price:    10,
			},
			{
				Product: models.Product{
					ID:          primitive.NewObjectID(),
					Name:        "Product 2",
					Description: "Product 2 description",
					Price:       20,
					Stock:       20,
				},
				Quantity: 4,
				Price:    80,
			},
		},
	}, nil
}

func (repository *MockedBasketRepositoryImpl) CreateBasicBasket() (*models.Basket, error) {
	return &models.Basket{
		ID:    primitive.NewObjectID(),
		Items: []models.BasketItem{},
	}, nil
}

func (repository *MockedBasketRepositoryImpl) GetAllBaskets() ([]models.Basket, error) {
	return []models.Basket{
		{
			ID: primitive.NewObjectID(),
			Items: []models.BasketItem{
				{
					Product: models.Product{
						ID:          primitive.NewObjectID(),
						Name:        "Product 1",
						Description: "Product 1 description",
						Price:       10,
						Stock:       10,
					},
					Quantity: 1,
					Price:    10,
				},
				{
					Product: models.Product{
						ID:          primitive.NewObjectID(),
						Name:        "Product 2",
						Description: "Product 2 description",
						Price:       20,
						Stock:       20,
					},
					Quantity: 4,
					Price:    80,
				},
			},
		},
	}, nil
}

func (repository *MockedBasketRepositoryImpl) GetBasketItems(basketID string) ([]models.BasketItem, error) {
	return []models.BasketItem{
		{
			Product: models.Product{
				ID:          primitive.NewObjectID(),
				Name:        "Product 1",
				Description: "Product 1 description",
				Price:       10,
				Stock:       10,
			},
			Quantity: 1,
			Price:    10,
		},
		{
			Product: models.Product{
				ID:          primitive.NewObjectID(),
				Name:        "Product 2",
				Description: "Product 2 description",
				Price:       20,
				Stock:       20,
			},
			Quantity: 4,
			Price:    80,
		},
	}, nil
}
