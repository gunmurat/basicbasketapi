package helpers

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/gunmurat/shopping-app/config"
)

func Initialize() {
	err := godotenv.Load("/Users/muratgun/Dev/GoProjects/BasicShoppingCart/.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	err = config.ConnectMongo()
	if err != nil {
		log.Fatal("Error connecting to mongo")
	}
}
