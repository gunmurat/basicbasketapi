package service

import (
	"gitlab.com/gunmurat/shopping-app/models"
	"gitlab.com/gunmurat/shopping-app/repository"
)

type ProductService interface {
	GetAllProducts() ([]models.Product, error)
	GetProduct(id string) (*models.Product, error)
	CreateProduct(body []byte) (*models.Product, error)
}

type ProductServiceImpl struct {
	repository repository.ProductRepository
}

func NewProductService(repository repository.ProductRepository) ProductService {
	return &ProductServiceImpl{
		repository: repository,
	}
}

func (service *ProductServiceImpl) GetAllProducts() ([]models.Product, error) {
	return service.repository.GetAllProducts()
}

func (service *ProductServiceImpl) GetProduct(id string) (*models.Product, error) {
	return service.repository.GetProduct(id)
}

func (service *ProductServiceImpl) CreateProduct(body []byte) (*models.Product, error) {
	return service.repository.CreateProduct(body)
}
