package service

import (
	"errors"

	"gitlab.com/gunmurat/shopping-app/models"
	"gitlab.com/gunmurat/shopping-app/repository"
)

type BasketService interface {
	AddProductToBasket(basketID string, body []byte) (bool, error)
	RemoveProductFromBasket(basketID string, productId string) (bool, error)
	GetBasket(basketID string) (*models.Basket, error)
	CreateBasicBasket() (*models.Basket, error)
	GetAllBaskets() ([]models.Basket, error)
	GetBasketItemById(basketId string, productId string) (*models.BasketItem, error)
}

type BasketServiceImpl struct {
	repository repository.BasketRepository
}

func NewBasketService(repository repository.BasketRepository) BasketService {
	return &BasketServiceImpl{
		repository: repository,
	}
}

func (service *BasketServiceImpl) AddProductToBasket(basketID string, body []byte) (bool, error) {
	return service.repository.AddProductToBasket(basketID, body)
}

func (service *BasketServiceImpl) GetBasket(basketID string) (*models.Basket, error) {
	return service.repository.GetBasket(basketID)
}

func (service *BasketServiceImpl) CreateBasicBasket() (*models.Basket, error) {
	return service.repository.CreateBasicBasket()
}

func (service *BasketServiceImpl) RemoveProductFromBasket(basketID string, productId string) (bool, error) {
	return service.repository.RemoveProductFromBasket(basketID, productId)
}

func (service *BasketServiceImpl) GetAllBaskets() ([]models.Basket, error) {
	return service.repository.GetAllBaskets()
}

func (service *BasketServiceImpl) GetBasketItemById(basketId string, productId string) (*models.BasketItem, error) {
	basket, err := service.repository.GetBasket(basketId)
	if err != nil {
		return nil, err
	}

	for _, item := range basket.Items {
		if item.Product.ID.Hex() == productId {
			return &item, nil
		}
	}

	return nil, errors.New("Basket item not found")
}
