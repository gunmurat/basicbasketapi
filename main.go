package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"gitlab.com/gunmurat/shopping-app/helpers"
	"gitlab.com/gunmurat/shopping-app/routes"
)

func init() {
	helpers.Initialize()
}

func main() {

	app := fiber.New()

	app.Use(logger.New())

	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowMethods: "GET, POST, PUT, DELETE, OPTIONS",
		AllowHeaders: "Content-Type, Authorization, Content-Length, X-Requested-With",
	}))

	routes.BasketRoutes(app)
	routes.ProductRoutes(app)

	app.Listen(":5000")

}
