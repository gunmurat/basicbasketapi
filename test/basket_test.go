package test

import (
	"testing"

	"gitlab.com/gunmurat/shopping-app/repository/mock"
	"gitlab.com/gunmurat/shopping-app/service"
	"gotest.tools/assert"
)

var mockedBasketService = service.NewBasketService(mock.NewMockedBasketRepository("baskets"))

func TestCreateBasicBasket(t *testing.T) {
	basket, err := mockedBasketService.CreateBasicBasket()

	assert.NilError(t, err)
	assert.Equal(t, len(basket.Items), 0)
}

func TestAddItemToBasket(t *testing.T) {
	basket, err := mockedBasketService.CreateBasicBasket()

	assert.NilError(t, err)
	assert.Equal(t, len(basket.Items), 0)

	res, _ := mockedBasketService.AddProductToBasket(basket.ID.Hex(), []byte(`{"productID":"6230e65c8ed47e30845fa20e", "quantity":2}`))

	assert.NilError(t, err)
	assert.Equal(t, res, true)
}

func TestRemoveItemFromBasket(t *testing.T) {
	basket, err := mockedBasketService.CreateBasicBasket()

	assert.NilError(t, err)
	assert.Equal(t, len(basket.Items), 0)

	res, _ := mockedBasketService.RemoveProductFromBasket(basket.ID.Hex(), "6230e65c8ed47e30845fa20e")

	assert.NilError(t, err)
	assert.Equal(t, res, true)
}

func TestGetBasicBasket(t *testing.T) {

	basket, err := mockedBasketService.GetBasket("6230e65c8ed47e30845fa20e")

	assert.NilError(t, err)
	assert.Equal(t, len(basket.Items), 2)
}
