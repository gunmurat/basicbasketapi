package test

import (
	"testing"

	"gitlab.com/gunmurat/shopping-app/repository/mock"
	"gitlab.com/gunmurat/shopping-app/service"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"gotest.tools/assert"
)

var mockedProductService = service.NewProductService(mock.NewMockedProductRepository("products"))

func TestGetAllProducts(t *testing.T) {
	products, err := mockedProductService.GetAllProducts()
	assert.NilError(t, err)
	assert.Equal(t, len(products), 2)
}

func TestGetProduct(t *testing.T) {
	product, err := mockedProductService.GetProduct("6230e65c8ed47e30845fa20e")

	hex, err := primitive.ObjectIDFromHex("6230e65c8ed47e30845fa20e")

	assert.NilError(t, err)
	assert.Equal(t, product.ID, hex)
	assert.Equal(t, product.Name, "Product 1")
	assert.Equal(t, product.Price, float64(10))
}

func TestCreateProduct(t *testing.T) {
	product, err := mockedProductService.CreateProduct([]byte(`{"name":"Product 3", "description":"Product 3 description", "price":30, "stock":30}`))

	assert.NilError(t, err)
	assert.Equal(t, product.Name, "Product 3")
	assert.Equal(t, product.Price, float64(30))
}
